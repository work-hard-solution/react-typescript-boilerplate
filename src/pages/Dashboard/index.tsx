import { DashboardLayout } from 'components/layout';
import React from 'react';

const DashboardPage: React.FC = () => {
    return <DashboardLayout />;
};

export default DashboardPage;
