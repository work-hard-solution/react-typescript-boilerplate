import { AuthLayout } from 'components/layout';
import { LoginContainer } from 'containers';
import React from 'react';

const LoginPage: React.FC = () => {
    return (
        <AuthLayout>
            <LoginContainer />
        </AuthLayout>
    );
};

export default LoginPage;
