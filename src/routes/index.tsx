import { EventDetail, Events } from 'containers/';
import React from 'react';
import { PATH } from 'utils/constants';

const LoginPage = React.lazy(() => import('pages/Auth/Login'));
const DashboardPage = React.lazy(() => import('pages/Dashboard'));
const NotFoundPage = React.lazy(() => import('pages/NotFound'));

export const routes = [
    {
        path: PATH.AUTH.LOGIN,
        component: LoginPage,
        auth: false,
        exact: true,
    },
    {
        path: PATH.NOTFOUND,
        component: NotFoundPage,
        auth: false,
        exact: true,
    },
    {
        path: '/admin',
        component: DashboardPage,
        auth: true,
        exact: false,
    },
];

export const adminRoutes = [
    {
        path: '/admin/dashboard',
        component: <Events />,
        auth: true,
        exact: true,
        // children: [
        //     {
        //         path: '/admin/dashboard/event/detail',
        //         component: <EventDetail />,
        //         auth: true,
        //         exact: false,
        //     },
        // ],
    },
];
