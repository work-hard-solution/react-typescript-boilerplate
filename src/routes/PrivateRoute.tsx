import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';

const PrivateRoute: React.FC<RouteProps> = ({ component, path, exact }) => {
    const isLoggedIn = Boolean(localStorage.getItem('access_token'));

    if (!isLoggedIn) return <Redirect to="/login" />;

    return <Route component={component} path={path} exact={exact} />;
};

export default PrivateRoute;
