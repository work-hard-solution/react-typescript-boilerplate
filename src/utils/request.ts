/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosResponse } from 'axios';
import { isEqual } from 'lodash';

import { METHODS } from './constants';

axios.defaults.baseURL = process.env.REACT_APP_BASE_URL_API;

const request = [METHODS.GET, METHODS.POST, METHODS.PUT, METHODS.DELETE].reduce((result: any, method: any) => {
    result[method] = async (url: string, data?: any, params?: any, headers?: any) => {
        const accessToken = window.localStorage.getItem('access_token');

        headers = {
            'Accept-Language': 'en',
        };

        headers['Content-Type'] = 'application/json';
        headers.Accept = 'application/json';

        if (accessToken) headers.Authorization = `Bearer ${accessToken}`;

        if (isEqual(method, METHODS.GET)) params = data;

        try {
            const response: AxiosResponse = await axios({
                method,
                url,
                data,
                headers,
                params,
            });

            return response;
        } catch (e: any) {
            throw e.message;
        }
    };
    return result;
}, {});

export default request;
