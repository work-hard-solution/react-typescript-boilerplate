export const TYPES = {
    FUNCTION: 'function',
    PASSWORD: 'password',
    PHONE: 'phone',
    TEXT: 'text',
    ACCESS_TOKEN: 'access_token',
    SEARCH: 'search',
};

export const PATH = {
    AUTH: {
        LOGIN: '/login',
        REGISTER: '/signup',
        FORGOT_PASSWORD: '/forgot-password',
        RESET_PASSWORD: '/reset-password',
    },
    DASHBOARD: '/dashboard',
    NOTFOUND: '/not-found',
};

export const METHODS = {
    GET: 'get',
    POST: 'post',
    PUT: 'put',
    PATCH: 'patch',
    DELETE: 'delete',
};
