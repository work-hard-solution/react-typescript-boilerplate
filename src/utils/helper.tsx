/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { capitalize, head, last, split } from 'lodash';
import moment from 'moment';

export const handleBreadCrumb = (pathname: any) => {
    return capitalize(last(split(pathname, '/', 3)));
};

export const formatNameAvatar = (name: any) => {
    const from = 'àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ';
    const to = 'aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy';
    for (let i = 0, l = from.length; i < l; i++) {
        name = name.replace(RegExp(from[i], 'gi'), to[i]);
    }

    const matches = name.match(/\b(\w)/g);
    const initials = `${head(matches)}${last(matches)}`.toLocaleUpperCase();
    return initials;
};

export const parseDate = (unixTime: any) => {
    const date = new Date(unixTime);
    return moment(date).format('MMMM Do YYYY, h:mm:ss a');
};
