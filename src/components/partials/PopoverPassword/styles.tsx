import { makeStyles } from '@mui/styles';
import { IPopoverPassword } from 'components/partials/PopoverPassword';

export const useStyles = makeStyles((theme: any) => ({
    wrapper: {
        paddingTop: ({ hasFocus }) => (hasFocus ? 10 : 0),
        transition: 'all 0.15s',
    },
    focusDescriptionsWrapper: {
        transition: 'all 0.15s',
        height: ({ hasFocus }) => (hasFocus ? 63 : 0),
        width: '100%',
        padding: ({ hasFocus }) => (hasFocus ? '0.75rem' : '0 0.75rem'),
        backgroundColor: 'rgb(242, 244, 247)',
        position: 'relative',
        border: ({ hasFocus }) => (hasFocus ? `1px  solid  #D3D5D8` : `0 solid #D3D5D8`),
        borderRadius: '0.875rem',
    },
    triangle: {
        transform: 'translateX(-50%) rotate(45deg)',
        transition: 'all 0.15s',
        position: 'absolute',
        opacity: ({ hasFocus }: IPopoverPassword) => (hasFocus ? 1 : 0),
        left: 25,
        top: -7,
        width: 14,
        height: 14,
        backgroundColor: 'rgb(242, 244, 247)',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: `#D3D5D8 transparent transparent
        #D3D5D8`,
    },
    descriptionText: {
        transition: 'all 0.15s',
        textAlign: 'left',
        color: '#000',
        opacity: ({ hasFocus }) => (hasFocus ? 1 : 0),
        margin: 0,
        fontFamily: theme.typography.medium,
        fontSize: 14,
        lineHeight: '19px',
        letterSpacing: 0,
    },
}));
