import { Box, Typography } from '@mui/material';
import React from 'react';

import { useStyles } from './styles';

export interface IPopoverPassword {
    hasFocus: boolean;
}

const PopoverPassword: React.FC<IPopoverPassword> = ({ hasFocus }) => {
    const classes = useStyles({ hasFocus });

    return (
        <div className={classes.wrapper}>
            <Box classes={{ root: classes.focusDescriptionsWrapper }}>
                <span className={classes.triangle} />
                <div className={classes.descriptionText}>
                    Password must contain a <strong>letter </strong> and a <strong>number</strong>, and minimum of
                    <strong> 9 characters</strong>
                </div>
            </Box>
        </div>
    );
};
export default PopoverPassword;
