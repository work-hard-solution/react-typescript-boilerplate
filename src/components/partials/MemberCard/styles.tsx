import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    memberCardWrapper: {
        borderRadius: '8px',
        padding: '8px',
        display: 'flex',
        marginBottom: '4px',
        '&:hover': {
            boxShadow: 'rgb(213 213 213 / 50%) 0px 2px 4px 0px',
            transform: 'scale(1.05) !important',
            transitionDuration: '0.4s',
        },
    },
    avatar: {
        marginRight: '8px !important',
    },
    infoMember: {},
    name: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '18px !important',
    },
    position: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
    },
}));
