import { Avatar, Typography } from '@mui/material';
import AOS from 'aos';
import React from 'react';

import { useStyles } from './styles';

interface IMemberCard {
    firstName: string;
    lastName: string;
    position: string;
    avatar: string;
}
const MemberCard: React.FC<IMemberCard> = ({ firstName, lastName, position, avatar }) => {
    const classes = useStyles();

    React.useEffect(() => {
        AOS.init({
            duration: 500,
        });
    }, []);

    return (
        <div className={classes.memberCardWrapper} data-aos-once="true" data-aos-easing="linear" data-aos="fade-down">
            <Avatar alt="Remy Sharp" src={avatar} className={classes.avatar} />
            <div className={classes.infoMember}>
                <Typography classes={{ root: classes.name }}>{`${firstName} ${lastName}`}</Typography>
                <Typography classes={{ root: classes.position }}>{position}</Typography>
            </div>
        </div>
    );
};
export default MemberCard;
