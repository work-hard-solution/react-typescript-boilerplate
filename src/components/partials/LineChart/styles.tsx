import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    title: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '18px !important',
    },
}));
