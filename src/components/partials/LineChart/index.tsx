import { Typography } from '@mui/material';
import React from 'react';
import { CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';

import { useStyles } from './styles';

const data = [
    {
        name: 'January',
        Iphone: 4000,
    },
    {
        name: 'March',
        Iphone: 1000,
    },
    {
        name: 'May',
        Iphone: 4000,
    },
    {
        name: 'July',
        Iphone: 800,
    },
    {
        name: 'October',
        Iphone: 1500,
    },
];

const Chart: React.FC = () => {
    return (
        <ResponsiveContainer width="100%" aspect={3}>
            <LineChart
                width={500}
                height={300}
                data={data}
                margin={{
                    top: 15,
                    right: 30,
                    left: 20,
                    bottom: 5,
                }}
            >
                <CartesianGrid stroke="#243240" />
                <XAxis dataKey="name" tick={{ fill: '#000', fontFamily: 'Avenir Next Demi !important' }} />
                <YAxis tick={{ fill: '#000' }} />
                <Tooltip
                    contentStyle={{
                        backgroundColor: '#8884d8',
                        color: '#000',
                        fontFamily: 'Avenir Next Demi !important',
                    }}
                    itemStyle={{ color: '#000', fontFamily: 'Avenir Next Demi !important' }}
                    cursor={false}
                />
                <Line
                    type="monotone"
                    dataKey="Iphone"
                    stroke="#8884d8"
                    strokeWidth="5"
                    dot={{ fill: '#2e4355', stroke: '#8884d8', strokeWidth: 2, r: 5 }}
                    activeDot={{ fill: '#2e4355', stroke: '#8884d8', strokeWidth: 5, r: 10 }}
                />
            </LineChart>
        </ResponsiveContainer>
    );
};

export default Chart;
