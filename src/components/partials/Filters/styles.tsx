import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
        borderRadius: '8px !important',
        '& .MuiInputBase-input': {
            fontSize: '14px !important',
            fontFamily: `${theme.typography.medium} !important`,
            lineHeight: '19px !important',
            borderRadius: '8px !important',
        },
        '& .MuiOutlinedInput-root': {
            borderRadius: '8px !important',
        },
        width: '50%',
        fontFamily: `${theme.typography.medium} !important`,
        '-webkit-tap-highlight-color': 'transparent !important',
        '&:hover': {
            border: 'none !importants',
            backgroundColor: 'transparent !important',
        },
        outline: 'none',
        letterSpacing: '0 !important',
        '&::placeholder': {
            fontSize: '12px !important',
            color: '#808080 !important',
            fontWeight: 500,
            letterSpacing: 0,
            lineHeight: '19px !important',
        },
    },
    searchIcon: {
        margin: '0 auto !important',
    },
}));
