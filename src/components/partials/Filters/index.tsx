import SearchIcon from '@mui/icons-material/Search';
import { Box, FormControl, Grid, InputAdornment, TextField } from '@mui/material';
import { ListParams } from 'models';
import React, { ChangeEvent } from 'react';

import { useStyles } from './styles';

interface IFilter {
    filter: ListParams;
    onChange?: (newFilter: ListParams) => void;
    onSearchChange?: (newFilter: ListParams) => void;
}

const Filters: React.FC<IFilter> = ({ filter, onChange, onSearchChange }) => {
    const classes = useStyles();
    const handleSearchChange = (e: ChangeEvent<HTMLInputElement>) => {
        if (!onSearchChange) return;
        const newFilter = {
            ...filter,
            search: e.target.value,
        };
        onSearchChange(newFilter);
    };

    return (
        <TextField
            type="search"
            id="standard-adornment-amount"
            onChange={handleSearchChange}
            variant="outlined"
            size="small"
            classes={{ root: classes.root }}
            placeholder="Search event by name..."
        />
    );
};

export default Filters;
