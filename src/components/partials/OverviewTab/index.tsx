import { Typography } from '@mui/material';
import clsx from 'clsx';
import React from 'react';

import { useStyles } from './styles';

interface IOverviewTab {
    name: string;
    calculator: string;
    className: string;
}

const OverviewTab: React.FC<IOverviewTab> = ({ children, name, calculator, className }) => {
    const classes = useStyles();
    return (
        <div className={clsx(classes.tab, className)}>
            {children}
            <div className={classes.titleWrapper}>
                <Typography>{calculator}</Typography>
                <Typography>{name}</Typography>
            </div>
        </div>
    );
};

export default OverviewTab;
