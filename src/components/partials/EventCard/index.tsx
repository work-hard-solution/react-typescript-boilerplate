import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Avatar, Card, CardContent, CardHeader, Divider, IconButton, Menu, MenuItem, Typography } from '@mui/material';
import AOS from 'aos';
import { capitalize } from 'lodash';
import React from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { parseDate } from 'utils/helper';

import { useStyles } from './styles';

interface IEventCard {
    id: string;
    eventName: string;
    unixTime: number;
    avatar: string;
    firstName: string;
    lastName: string;
    position: string;
    location: string;
}

const EventCard: React.FC<IEventCard> = ({
    id,
    eventName,
    unixTime,
    avatar,
    firstName,
    lastName,
    position,
    location,
}) => {
    const classes = useStyles();
    const history = useHistory();
    const match = useRouteMatch();
    React.useEffect(() => {
        AOS.init({
            duration: 500,
        });
    }, []);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const renderTime = () => {
        return (
            <Typography variant="h5" className={classes.date} component="p">
                {parseDate(unixTime)}
            </Typography>
        );
    };

    return (
        <Card className={classes.root} data-aos-once="true" data-aos-easing="linear" data-aos="fade-down">
            <CardHeader
                action={
                    <IconButton aria-label="settings" onClick={handleClick}>
                        <MoreVertIcon />
                    </IconButton>
                }
            />

            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <MenuItem onClick={handleClose}>Feed back</MenuItem>
                <MenuItem onClick={() => history.push(`${match.url}/${id}`)}>Detail</MenuItem>
                <MenuItem onClick={handleClose}>Remove</MenuItem>
            </Menu>

            <CardContent className={classes.cardContentWrapper}>
                <Typography gutterBottom variant="h3" component="div" classes={{ root: classes.eventName }}>
                    {capitalize(eventName)}
                </Typography>
                <div className={classes.infoWrapper}>
                    <div className={classes.nameWrapper}>
                        <Avatar aria-label="recipe" className={classes.avatar} variant="square" src={avatar} />
                        <div className={classes.info}>
                            <Typography classes={{ root: classes.position }} variant="overline">
                                {position}
                            </Typography>
                            <Typography variant="h5">{`${firstName} ${lastName}`}</Typography>
                        </div>
                    </div>
                    <Divider className={classes.divider} orientation="vertical" />
                    <div className={classes.dateWrapper}>
                        <Typography classes={{ root: classes.position }} variant="overline">
                            Date
                        </Typography>

                        {renderTime()}
                    </div>
                    <Divider className={classes.divider} orientation="vertical" />
                    <div className={classes.locationWrapper}>
                        <Typography classes={{ root: classes.position }} variant="overline">
                            Location
                        </Typography>
                        <a className={classes.location} href={location}>
                            Join link
                        </a>
                    </div>
                </div>
            </CardContent>
        </Card>
    );
};
export default EventCard;
