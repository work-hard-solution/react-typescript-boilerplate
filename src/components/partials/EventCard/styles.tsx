import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'row-reverse',
        width: '100%',
        justifyContent: 'space-between',
        marginBottom: '24px',
        marginRight: 'auto',
        marginLeft: 'auto',
        boxShadow: 'rgb(213 213 213 / 50%) -1px 2px 4px 1px !important',
        borderRadius: '8px !important',
        '&:hover': {
            transform: 'scale(1.02) !important',
            transitionDuration: '0.4s !important',
        },
    },
    date: {
        marginBottom: '8px !important',
    },
    avatar: {
        borderRadius: '30% !important',
        marginRight: '12px !important',
    },
    cardContentWrapper: {
        textAlign: 'left',
    },
    eventName: {
        marginBottom: '20px !important',
    },
    divider: {
        marginRight: '30px !important',
        marginLeft: '30px !important',
        height: '50px !important',
    },
    coachingInfo: {
        display: 'flex',
    },
    infoWrapper: {
        width: '105% !important',
        display: 'flex',
        justifyContent: 'space-between',
    },
    info: {},
    nameWrapper: {
        width: '265px !important',
        display: 'flex',
    },
    dateWrapper: {
        width: '260px !important',
    },
    position: {
        marginBottom: '4px !important',
    },
    locationWrapper: {
        display: 'flex',
        flexDirection: 'column',
    },
    location: {
        fontFamily: 'Avenir Next Demi',
        fontSize: 16,
        color: 'rgb(47, 129, 252)',
    },
}));
