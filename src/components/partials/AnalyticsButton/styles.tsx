import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    buttonAnalyticsWrapper: {
        width: '25%',
    },
    analytics: {
        width: '100%',
        border: '1px solid rgb(211, 213, 216) !important',
        borderRadius: '8px !important',
        justifyContent: 'space-between !important',
        display: 'flex !important',
        alignItems: 'center !important',
    },
    fontIcon: {
        display: 'flex !important',
        alignItems: 'center !important',
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '14px !important',
    },
    filterIcon: {
        marginRight: '12px !important',
    },
    keyboardIcon: {
        fontSize: '16px !important',
    },
}));
