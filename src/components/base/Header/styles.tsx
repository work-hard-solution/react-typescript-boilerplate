import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        backgroundColor: 'transparent !important',
        boxShadow: 'none !important',
    },
    title: {
        flexGrow: 1,
        fontFamily: theme.typography.demi,
        color: '#545458',
    },
}));
