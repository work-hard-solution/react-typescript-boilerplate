import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    buttonMenu: {},
    keyboardArrowDown: {
        color: '#545458',
    },
    avatar: {
        marginRight: '15px',
    },
    avatarContainer: {
        margin: '24px auto 20px auto',
    },
    nameMenu: {
        fontSize: '18px !important',
        fontFamily: `${theme.typography.demi} !important`,
        color: theme.palette.common.black,
    },
    email: {
        fontSize: '14px !important',
        fontFamily: `${theme.typography.regular} !important`,
        color: 'rgba(12,25,45,0.6)',
    },
    menuItem: {
        textAlign: 'center',
    },
    logoutButton: {
        marginBottom: '10px !important',
        border: '1px solid rgba(12,25,45,0.11) !important',
        width: '160px',
        fontFamily: `${theme.typography.demi} !important`,
    },
    mainAvatar: {
        fontSize: '36px !important',
        margin: '0 auto',
    },
    manageAccountButton: {
        marginTop: '10px !important',
        marginBottom: '10px !important',
        color: 'rgb(12, 25, 45) !important',
        fontFamily: `${theme.typography.demi} !important`,
    },
}));
