import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { Button, Typography } from '@mui/material';
import Divider from '@mui/material/Divider';
import Menu, { MenuProps } from '@mui/material/Menu';
import { alpha, styled } from '@mui/material/styles';
import { useAppDispatch } from 'app/hooks';
import { Avatar } from 'components/base';
import { capitalize } from 'lodash';
import React from 'react';
import { authActions } from 'redux/slices/auth';

import { useStyles } from './styles';

interface ICustomizedMenus {
    label: string;
    email: string;
}

const StyledMenu = styled((props: MenuProps) => (
    <Menu
        elevation={2}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
        }}
        {...props}
    />
))(({ theme }) => ({
    '& .MuiPaper-root': {
        width: 295,
        borderRadius: 4,
        marginTop: theme.spacing(1),
        minWidth: 180,
        color: theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
        border: '1px solid #D3D5D8',
        boxShadow:
            'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
        '& .MuiMenu-list': {
            padding: '4px 0',
        },
        '& .MuiMenuItem-root': {
            '& .MuiSvgIcon-root': {
                fontSize: 18,
                color: theme.palette.text.secondary,
                marginRight: theme.spacing(1.5),
            },
            '&:active': {
                backgroundColor: alpha(theme.palette.primary.main, theme.palette.action.selectedOpacity),
            },
        },
    },
}));

export const CustomizedMenus: React.FC<ICustomizedMenus> = ({ label, email }) => {
    const classes = useStyles();
    const dispatch = useAppDispatch();

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const open = Boolean(anchorEl);

    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        dispatch(authActions.logout());
    };

    return (
        <div>
            <Button
                classes={{ root: classes.buttonMenu }}
                id="demo-customized-button"
                aria-controls="demo-customized-menu"
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                variant="text"
                disableElevation
                onClick={handleClick}
                endIcon={<KeyboardArrowDownIcon className={classes.keyboardArrowDown} />}
            >
                <Avatar className={classes.avatar}>{label}</Avatar>
                <Typography variant="body2">{capitalize(label)}</Typography>
            </Button>
            <StyledMenu
                id="demo-customized-menu"
                MenuListProps={{
                    'aria-labelledby': 'demo-customized-button',
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
            >
                <div className={classes.menuItem}>
                    <div className={classes.avatarContainer}>
                        <Avatar className={classes.mainAvatar} width={80} height={80}>
                            {label}
                        </Avatar>
                    </div>
                    <Typography classes={{ root: classes.nameMenu }}>{label}</Typography>
                    <Typography classes={{ root: classes.email }}>{email}</Typography>
                    <Button className={classes.manageAccountButton}>Manage your CoachingCloud account</Button>
                    <Button color="inherit" onClick={handleLogout} className={classes.logoutButton}>
                        Logout
                    </Button>
                </div>
            </StyledMenu>
        </div>
    );
};
export default CustomizedMenus;
