import { AppBar, Toolbar, Typography } from '@mui/material';
import { useAppSelector } from 'app/hooks';
import React from 'react';
import { useLocation } from 'react-router';
import { handleBreadCrumb } from 'utils/helper';

import MenuNavBar from './MenuNavbar';
import { useStyles } from './styles';

const Header: React.FC = () => {
    const classes = useStyles();

    const location = useLocation();

    const title = handleBreadCrumb(location.pathname);

    const userInfo: any = useAppSelector((state) => state.user.currentUser);

    return (
        <div className={classes.root}>
            <AppBar position="static" classes={{ root: classes.appBar }}>
                <Toolbar>
                    <Typography variant="h4" className={classes.title}>
                        {title}
                    </Typography>

                    <MenuNavBar label={userInfo?.name} email={userInfo?.email} />
                </Toolbar>
            </AppBar>
        </div>
    );
};

export default Header;
