import { Button as MuiButton, CircularProgress } from '@mui/material';
import clsx from 'clsx';
import React, { ElementType } from 'react';

import { useStyles } from './styles';

enum Type {
    submit = 'submit',
    button = 'button',
    reset = 'rest',
}
interface IButton {
    className?: string;
    loading?: boolean;
    disabled?: boolean;
    startIcon?: ElementType;
    onClick?: () => void;
    type?: keyof typeof Type;
    src?: string;
    alt?: string;
}

const Button: React.FC<IButton> = ({ className, loading, startIcon, disabled, onClick, children, type, src, alt }) => {
    const classes = useStyles();

    return (
        <MuiButton
            disabled={disabled}
            className={clsx(className, classes.customButton)}
            startIcon={loading ? <CircularProgress className={classes.spinner} size="1rem" /> : startIcon}
            onClick={onClick}
            variant="text"
            type={type}
        >
            {src ? <img alt={alt} src={src} /> : children}
        </MuiButton>
    );
};

export default Button;
