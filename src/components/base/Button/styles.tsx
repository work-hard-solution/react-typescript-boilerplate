import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    customButton: {
        fontFamily: theme.typography.medium,
    },
    spinner: {},
}));
