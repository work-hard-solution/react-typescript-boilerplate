import { Link as MuiLink } from '@mui/material';
import clsx from 'clsx';
import React from 'react';
import { Link as LinkRouter } from 'react-router-dom';

import { useStyles } from './styles';

interface ILink {
    to: string;
    blank: boolean;
    className?: string;
}

const Link: React.FC<ILink> = ({ blank, children, to, className }) => {
    const classes = useStyles();

    return blank ? (
        <MuiLink className={clsx(classes.root, className)}>{children}</MuiLink>
    ) : (
        <LinkRouter className={clsx(classes.root, className)} to={to}>
            {children}
        </LinkRouter>
    );
};

export default Link;
