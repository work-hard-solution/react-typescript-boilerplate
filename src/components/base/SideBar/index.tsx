import { Box, Divider, List, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { images } from 'assets/images';
import { isEqual } from 'lodash';
import React from 'react';
import { NavLink, useLocation } from 'react-router-dom';

import { sidebarItem } from './const';
import NavigationExpend from './NavigationExpend';
import { useStyles } from './styles';

const Sidebar: React.FC = () => {
    const location = useLocation();

    const isActive = !!location.pathname;

    console.log('location___', location);
    const classes = useStyles({ isActive });

    console.log('isActive___', isActive);

    return (
        <Box className={classes.root}>
            <nav aria-label="main mailbox folders">
                <div className={classes.logoWrapper}>
                    <img src={images.main_logo.src} alt="coaching-logo" className={classes.logo} />
                </div>
                <List>
                    {sidebarItem.map(({ id, to, name, icon, alt }) => (
                        <NavLink to={to} key={id} className={classes.itemText}>
                            <ListItem disablePadding className={classes.listItem}>
                                {isEqual(to, location?.pathname) && (
                                    <span className={classes.containerDivider}>
                                        <Divider orientation="vertical" />
                                    </span>
                                )}
                                <ListItemButton className={classes.listItemButton}>
                                    <ListItemIcon>
                                        <img src={icon} alt={alt} />
                                    </ListItemIcon>
                                    <ListItemText primary={name} className={classes.nameItem} />
                                </ListItemButton>
                            </ListItem>
                        </NavLink>
                    ))}
                </List>
            </nav>
            <NavigationExpend />
        </Box>
    );
};

export default Sidebar;
