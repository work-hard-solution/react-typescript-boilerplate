import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
        width: '100%',
    },
    logoWrapper: {
        width: '100%',
        textAlign: 'center',
        marginTop: '32px',
        marginBottom: '64px',
    },
    logo: {
        width: '80%',
        margin: 'auto',
    },
    itemText: {
        '&:link': {
            textDecoration: 'none',
        },
    },
    listItem: {
        height: '48px',
        '& .MuiListItemButton-root': {
            padding: '16px',
        },
    },
    containerDivider: {
        height: '48px',
        width: 6,
        backgroundColor: 'rgb(87, 202, 191) !important',
    },
    listItemButton: {
        height: '48px',
        backgroundColor: ({ isActive }: any) => isActive && 'rgb(45, 87, 186) !important',
    },
    nameItem: {
        '& .MuiTypography-root': {
            fontFamily: theme.typography.demi,
            color: 'rgba(250, 250, 250, 1)',
            lineHeight: '19px',
            fontSize: '15px',
            textDecoration: 'none',
        },
    },
}));
