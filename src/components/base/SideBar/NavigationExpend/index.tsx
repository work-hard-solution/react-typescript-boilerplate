import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Typography } from '@mui/material';
import { useAppSelector } from 'app/hooks';
import React from 'react';

import { useStyles } from './styles';

const NavigationExpend: React.FC = () => {
    const classes = useStyles({ detailsMaxHeight: window.innerHeight - 400 });

    return (
        <Accordion classes={{ root: classes.containerRoot, expanded: classes.containerExpanded }} square>
            <AccordionSummary
                classes={{
                    root: classes.summaryRoot,
                }}
                expandIcon={<ExpandMoreIcon className={classes.expendIcon} />}
            >
                <Typography classes={{ root: classes.management }}>Management your workspace</Typography>
            </AccordionSummary>
            <AccordionDetails classes={{ root: classes.detailsRoot }}>
                <Box display="flex" justifyContent="center" p={3}>
                    <Button color="secondary" size="small">
                        Create organization
                    </Button>
                </Box>
            </AccordionDetails>
        </Accordion>
    );
};

export default NavigationExpend;
