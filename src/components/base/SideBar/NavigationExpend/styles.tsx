import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    containerRoot: {
        backgroundColor: 'rgb(45, 87, 186) !important',
        boxShadow: 'none !important',
        '&::before': {
            display: 'none !important',
        },
    },
    containerExpanded: {
        margin: '0 !important',
    },
    summaryRoot: {
        padding: '8px !important',
        minHeight: '49px !important',
        '& > *': {
            overflow: 'hidden',
        },
    },
    detailsRoot: {
        overflowX: 'hidden',
        overflowY: 'auto',
        maxHeight: ({ detailsMaxHeight }: any) => detailsMaxHeight,
        // '&::-webkit-scrollbar': {
        //     width: 6,
        // },
        // '&::-webkit-scrollbar-thumb': {
        //     backgroundColor: theme.palette.highlight.main,
        //     borderRadius: 4,
        // },
    },
    expendIcon: {
        fontSize: '20px !important',
        color: 'rgba(250, 250, 250, 1)',
    },
    management: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: 14,
        color: 'rgba(250, 250, 250, 1)',
        lineHeight: '19px',
    },
}));
