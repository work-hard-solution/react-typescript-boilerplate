import { images } from 'assets/images';
import ClientIcon from 'assets/images/icons/clients.svg';
import TimeSheet from 'assets/images/icons/timesheets.svg';

export const sidebarItem = [
    {
        id: 'home',
        to: '/admin/dashboard',
        name: 'Home',
        icon: images.home_icon.src,
        alt: 'home-icon',
    },
    {
        id: 'events',
        to: '/admin/events',
        name: 'Events',
        icon: images.event_icon.src,
        alt: 'event-icon',
    },
    {
        id: 'Projects',
        to: '/admin/project',
        name: 'Projects',
        icon: images.project_icon.src,
        alt: 'project-icon',
    },
    {
        id: 'Client',
        to: '/admin/client',
        name: 'Clients',
        icon: ClientIcon,
        alt: 'client-icon',
    },
    {
        id: 'TimeSheet',
        to: '/admin/time-sheet',
        name: 'Time Sheet',
        icon: TimeSheet,
        alt: 'Time sheets',
    },
];
