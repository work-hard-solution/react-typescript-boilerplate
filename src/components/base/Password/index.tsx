import { Visibility, VisibilityOff } from '@mui/icons-material';
import { IconButton, InputAdornment, TextField as MuiTextField } from '@mui/material';
import clsx from 'clsx';
import { PopoverPassword } from 'components/partials';
import React, { InputHTMLAttributes, useState } from 'react';
import { Control, useController } from 'react-hook-form';

import { useStyles } from './styles';

interface IInput extends InputHTMLAttributes<HTMLInputElement> {
    name: string;
    // error?: boolean;
    // helperText?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    control: Control<any>;
    className?: string;
}

const Password: React.FC<IInput> = ({ name, control, className }) => {
    const classes = useStyles();

    const [showPassword, setShowPassword] = useState(false);
    const [hasFocus, setFocus] = useState(false);

    // const handleFocus = (e: any) => {
    //     setFocus(true);
    //     if (isEqual(typeof onFocus, TYPES.FUNCTION)) {
    //         onFocus(e);
    //     }
    // };
    const handleClickShowPassword = () => {
        setShowPassword(true);
    };

    const {
        field: { value, onChange, onBlur, ref },
        fieldState: { invalid, error },
    } = useController({ name, control });

    return (
        <div className={classes.passwordWrapper}>
            <MuiTextField
                onChange={onChange}
                onBlur={onBlur}
                value={value}
                name={name}
                type="password"
                placeholder="Your password"
                size="small"
                error={invalid}
                helperText={error?.message}
                className={clsx(classes.passwordContainer, className)}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                edge="end"
                            >
                                {showPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                        </InputAdornment>
                    ),
                }}
                classes={{ root: classes.password }}
                inputRef={ref}
            />
            <PopoverPassword hasFocus={error ? true : false || false} />
        </div>
    );
};

export default Password;
