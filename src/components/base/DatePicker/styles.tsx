import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    datePickerWrapper: {
        width: '80%',
        boxShadow: 'rgb(213 213 213 / 50%) -1px 2px 4px 1px',
        borderRadius: 8,
        marginBottom: '36px',
    },
    title: {
        flexGrow: 1,
    },
}));
