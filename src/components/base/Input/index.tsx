import { TextField as MuiTextField } from '@mui/material';
import clsx from 'clsx';
import React, { InputHTMLAttributes } from 'react';
import { Control, useController } from 'react-hook-form';

import { useStyles } from './styles';

interface IInput extends InputHTMLAttributes<HTMLInputElement> {
    name: string;
    type: string;
    // error?: boolean;
    // helperText?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    control: Control<any>;
    className?: string;
    placeholder?: string;
}

const Input: React.FC<IInput> = ({ name, type, className, placeholder, control }) => {
    const classes = useStyles();

    const {
        field: { value, onChange, onBlur, ref },
        fieldState: { invalid, error },
    } = useController({ name, control });

    return (
        <MuiTextField
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            type={type}
            error={invalid}
            placeholder={placeholder}
            helperText={error?.message}
            className={clsx(classes.inputWrapper, className)}
            inputRef={ref}
            size="small"
            classes={{ root: classes.input }}
        />
    );
};

export default Input;
