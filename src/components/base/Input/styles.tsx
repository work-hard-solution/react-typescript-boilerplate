// import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
    inputWrapper: {},
    input: {
        '& .MuiOutlinedInput-root': {
            borderRadius: '8px !important',
        },
        '&::placeholder': {
            fontSize: '12px !important',
            color: '#808080 !important',
            fontWeight: 500,
            letterSpacing: 0,
            lineHeight: '19px !important',
        },
    },
}));
