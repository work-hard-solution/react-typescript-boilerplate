import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

import { IAvatar } from './index';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
        borderRadius: ({ radius }: IAvatar) => radius,
        color: theme.palette.common.white,
        backgroundColor: `${theme.palette.primary.contrastText} !important`,
        fontSize: ({ size }: any) => `${size / 2 - 3} !important`,
        fontFamily: `${theme.typography.demi} !important`,
        textShadow: '0 2px 2px rgba(0,0,0,0.5) !important',
    },
}));
