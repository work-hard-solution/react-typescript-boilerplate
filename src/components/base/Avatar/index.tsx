import { Avatar as MuiAvatar } from '@mui/material';
import React from 'react';
import { formatNameAvatar } from 'utils/helper';

import { useStyles } from './styles';

export interface IAvatar {
    alt?: string;
    width?: number;
    height?: number;
    sizes?: string;
    size?: number;
    radius?: number;
    srcSet?: string;
    className?: string;
}

const Avatar: React.FC<IAvatar> = ({ alt, width, height, size, sizes, children, radius, srcSet, className }) => {
    const classes = useStyles({ size, radius });
    const [source, setSource] = React.useState(false);

    return (
        <MuiAvatar
            sx={{ width: width, height: height }}
            className={className}
            classes={{ root: classes.root }}
            alt={alt}
            sizes={sizes}
            srcSet={srcSet}
            imgProps={{
                onLoad: () => setSource(true),
                onError: () => setSource(false),
            }}
        >
            {!source && !!children && formatNameAvatar(children)}
        </MuiAvatar>
    );
};

export default Avatar;
