import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    dashboardWrapper: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
    },
    helloWrapper: {
        width: '100%',
    },
    paperHello: {
        width: '32%',
        height: '768px',
        borderRadius: '12px !important',
    },
    hello: {
        width: '90%',
        padding: '32px',
        borderRadius: '8px',
        backgroundColor: 'rgb(217, 232, 255)',
        margin: '24px auto 128px auto',
    },
    helloTitle: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '32px !important',
        color: theme.palette.common.black,
        marginBottom: '20px !important',
    },
    caption: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
        letterSpacing: '0 !important',
        lineHeight: '19px !important',
    },
    yourProcessWrapper: {
        marginTop: '24px',
        marginBottom: '24px',
    },
    subCaption: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
        letterSpacing: '0 !important',
        lineHeight: '19px !important',
        marginBottom: '8px !important',
    },
    status: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '22px !important',
        letterSpacing: '0 !important',
        lineHeight: '19px !important',
    },
    scheduleButton: {
        color: `${theme.palette.common.white} !important`,
        backgroundColor: 'rgb(47, 129, 252) !important',
        fontSize: '14px !important',
        textAlign: 'center',
        width: '100%',
        padding: '12px !important',
        borderRadius: '8px !important',
        fontFamily: `${theme.typography.demi} !important`,
        '&:hover': {
            backgroundColor: 'rgb(24, 102, 219) !important',
        },
    },
    backgroundWrapper: {
        width: '100%',
        textAlign: 'center',
    },
    background: {
        width: '95%',
        [theme.breakpoints.up(1800)]: {
            width: '70%',
        },
    },
    overviewWrapper: {
        width: '32%',
    },
    paperOverview: {
        width: '100%',
        padding: '24px',
        borderRadius: '12px !important',
        marginBottom: '56px',
    },
    overviewTab: {
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        margin: 'auto',
    },
    time: {
        backgroundColor: 'rgb(58, 173, 234)',
        color: 'rgb(7, 68, 102)',
        display: 'flex',
        borderRadius: '16px',
        padding: '24px',
        width: '45%',
        justifyContent: 'center',
        marginBottom: '86px',
    },
    titleTime: {
        color: 'rgb(7, 68, 102)',
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '28px !important',
    },
    timeIcon: {
        color: 'rgb(7, 68, 102)',
        margin: 'auto 10px auto 0',
        fontSize: '28px !important',
    },
    meetings: {
        display: 'flex',
        borderRadius: '16px',
        padding: '24px',
        width: '45%',
        justifyContent: 'center',
        marginBottom: '86px',
        backgroundColor: 'rgb(87, 202, 191)',
    },
    meetingIcon: {
        color: 'rgb(13, 112, 103)',
        margin: 'auto 10px auto 0',
        fontSize: '28px !important',
    },
    titleMeetings: {
        color: 'rgb(13, 112, 103)',
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '28px !important',
    },
    attended: {
        display: 'flex',
        borderRadius: '16px',
        padding: '24px',
        width: '45%',
        justifyContent: 'center',
        backgroundColor: 'rgb(249, 146, 82)',
    },
    titleAttended: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '28px !important',
        color: 'rgb(176, 88, 33) !important',
    },
    groupIcon: {
        color: 'rgb(176, 88, 33) !important',
        fontSize: '28px !important',
        margin: 'auto 10px auto 0',
    },
    reject: {
        display: 'flex',
        borderRadius: '16px',
        padding: '24px',
        width: '45%',
        justifyContent: 'center',
        backgroundColor: 'rgb(191, 107, 255)',
    },
    cancelIcon: {
        fontSize: '28px !important',
        color: 'rgb(144, 48, 217) !important',
        margin: 'auto 10px auto 0',
    },
    titleReject: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '28px !important',
        color: 'rgb(144, 48, 217) !important',
    },
    chartWrapper: {},
    scheduleWrapper: {
        width: '30%',
    },
    nameTab: {
        color: `${theme.palette.common.black} !important`,
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '14px !important',
        textAlign: 'center',
    },
    paperAttend: {
        width: '100%',
        padding: '24px',
        borderRadius: '12px !important',
    },
    attendMeeting: {
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: '32px',
    },
    attendTitle: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '22px !important',
    },
    hours: {
        color: 'rgb(30, 189, 130)',
    },
    attendCaption: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
    },
    chartTitle: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '18px !important',
        textAlign: 'center',
        marginBottom: '30px !important',
    },
    paperMember: {
        width: '100%',
        padding: '24px',
        borderRadius: '12px !important',
        marginBottom: '36px',
    },
}));
