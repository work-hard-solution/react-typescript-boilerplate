import AccessTimeIcon from '@mui/icons-material/AccessTime';
import CancelPresentationIcon from '@mui/icons-material/CancelPresentation';
import DateRangeIcon from '@mui/icons-material/DateRange';
import GroupIcon from '@mui/icons-material/Group';
import { Avatar, AvatarGroup, Button, Paper, Typography } from '@mui/material';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import DashboardBackground from 'assets/images/Dashboard-background.svg';
import { LineChart, MemberCard } from 'components/partials';
import React from 'react';
import { eventActions, selectEventList } from 'redux/slices/event';

import { useStyles } from './styles';

const HomeLayout: React.FC = () => {
    const classes = useStyles();
    const dispatch = useAppDispatch();
    const eventList = useAppSelector(selectEventList);

    React.useEffect(() => {
        dispatch(eventActions.fetchEventList({ limit: 10, page: 1 }));
    }, [dispatch]);

    return (
        <div className={classes.dashboardWrapper}>
            <Paper elevation={0} classes={{ root: classes.paperHello }}>
                <div className={classes.helloWrapper}>
                    <div className={classes.hello}>
                        <Typography classes={{ root: classes.helloTitle }}>Hi, Luu P.</Typography>
                        <Typography classes={{ root: classes.caption }}>
                            You have 6 meetings finish in this week.
                        </Typography>
                        <Typography classes={{ root: classes.caption }}>Your process activity is excellent</Typography>
                        <div className={classes.yourProcessWrapper}>
                            <Typography classes={{ root: classes.subCaption }}>Your Process</Typography>
                            <Typography classes={{ root: classes.status }}>Awesome</Typography>
                        </div>
                        <Button classes={{ root: classes.scheduleButton }}>View Schedule</Button>
                    </div>
                    <div className={classes.backgroundWrapper}>
                        <img src={DashboardBackground} alt="dashboard-background" className={classes.background} />
                    </div>
                </div>
            </Paper>
            <div className={classes.overviewWrapper}>
                <Paper elevation={0} classes={{ root: classes.paperOverview }}>
                    <div className={classes.overviewTab}>
                        <div className={classes.time}>
                            <AccessTimeIcon className={classes.timeIcon} />
                            <div>
                                <Typography classes={{ root: classes.titleTime }}>40K</Typography>
                                <Typography classes={{ root: classes.nameTab }}>Time</Typography>
                            </div>
                        </div>
                        <div className={classes.meetings}>
                            <DateRangeIcon className={classes.meetingIcon} />
                            <div>
                                <Typography classes={{ root: classes.titleMeetings }}>500</Typography>
                                <Typography classes={{ root: classes.nameTab }}>Meetings</Typography>
                            </div>
                        </div>
                        <div className={classes.attended}>
                            <GroupIcon className={classes.groupIcon} />
                            <div>
                                <Typography classes={{ root: classes.titleAttended }}>20K</Typography>
                                <Typography classes={{ root: classes.nameTab }}>Attended</Typography>
                            </div>
                        </div>
                        <div className={classes.reject}>
                            <CancelPresentationIcon className={classes.cancelIcon} />
                            <div>
                                <Typography classes={{ root: classes.titleReject }}>20</Typography>
                                <Typography classes={{ root: classes.nameTab }}>Reject</Typography>
                            </div>
                        </div>
                    </div>
                </Paper>
                <Paper elevation={0} classes={{ root: classes.paperAttend }}>
                    <div className={classes.chartWrapper}>
                        <div className={classes.attendMeeting}>
                            <div>
                                <Typography classes={{ root: classes.attendTitle }}>Attended Meetings</Typography>
                                <Typography classes={{ root: classes.attendCaption }}>
                                    <span className={classes.hours}>20 hours </span>
                                    call this months.
                                </Typography>
                            </div>
                            <AvatarGroup max={4}>
                                <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                                <Avatar alt="Travis Howard" src="/static/images/avatar/2.jpg" />
                                <Avatar alt="Cindy Baker" src="/static/images/avatar/3.jpg" />
                            </AvatarGroup>
                        </div>
                        <div>
                            <Typography classes={{ root: classes.chartTitle }}>
                                Quarterly sales figures for mobile phones
                            </Typography>
                            <LineChart />
                        </div>
                    </div>
                </Paper>
            </div>
            <div className={classes.scheduleWrapper}>
                <Paper elevation={0} classes={{ root: classes.paperMember }}>
                    <Typography classes={{ root: classes.attendTitle }}>Members</Typography>
                    {eventList.map((item) => (
                        <MemberCard
                            key={item?.id}
                            lastName={item?.lastName}
                            firstName={item?.firstName}
                            position={item?.role}
                            avatar={item?.avatar}
                        />
                    ))}
                </Paper>
            </div>
        </div>
    );
};
export default HomeLayout;
