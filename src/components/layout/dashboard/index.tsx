import { Box } from '@mui/material';
import { useAppDispatch } from 'app/hooks';
import { Header, Sidebar } from 'components/base';
import { EventLayout, HomeLayout } from 'components/layout';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { userActions } from 'redux/slices/user';

import { useStyles } from './styles';

const DashboardLayout: React.FC = () => {
    const classes = useStyles();

    const dispatch = useAppDispatch();

    React.useEffect(() => {
        dispatch(userActions.getUserInfo());
    }, []);

    return (
        <Box className={classes.root}>
            <Box className={classes.sidebar}>
                <Sidebar />
            </Box>

            <Box className={classes.main}>
                <Box className={classes.header}>
                    <Header />
                </Box>
                <Box className={classes.subMain}>
                    <Switch>
                        <Route path="/admin/dashboard">
                            <HomeLayout />
                        </Route>
                        <Route path="/admin/events">
                            <EventLayout />
                        </Route>
                    </Switch>
                </Box>
            </Box>
        </Box>
    );
};

export default DashboardLayout;
