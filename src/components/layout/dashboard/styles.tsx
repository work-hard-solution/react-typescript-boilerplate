import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'grid',
        gridTemplateRows: 'auto 1fr',
        gridTemplateColumns: '280px 1fr',
        gridTemplateAreas: `"header header" "sidebar main"`,
        minHeight: '100vh',
    },
    header: {
        gridArea: 'header',
        backgroundColor: theme.palette.common.white,
        boxShadow: 'rgb(213 213 213 / 50%) 0px 0px 4px 0px',
    },
    sidebar: {
        gridArea: 'sidebar',
        backgroundColor: theme.palette.primary.dark,
    },
    main: {
        backgroundColor: '#F2F5F7',
        gridArea: 'main',
    },
    subMain: {
        padding: theme.spacing(2, 3),
    },
}));
