import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import EventIcon from '@mui/icons-material/Event';
import { Avatar, Button, Divider, Paper, Typography } from '@mui/material';
import TextField from '@mui/material/TextField';
import { IEvent } from 'models';
import React from 'react';
import { Link, useParams } from 'react-router-dom';
import eventApi from 'redux/apis/event';
import { parseDate } from 'utils/helper';

import { useStyles } from './styles';

const EventDetail: React.FC = () => {
    const classes = useStyles();
    const { eventId } = useParams<{ eventId: string }>();
    const [event, setEvent] = React.useState<IEvent>();

    React.useEffect(() => {
        if (!eventId) return;
        //IFFE
        (async () => {
            try {
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                const response: any = await eventApi.getById(eventId);
                setEvent(response.data);
            } catch (err) {
                console.log('Failed to fetch event by id', err);
            }
        })();
    }, [eventId]);

    return (
        <div className={classes.eventDetailWrapper}>
            <Link to="/admin/events" style={{ textDecoration: 'none' }}>
                <Typography classes={{ root: classes.backToEvent }}>
                    <ArrowBackIcon className={classes.arrowBackIcon} /> Back to event list
                </Typography>
            </Link>

            <div>
                <Typography classes={{ root: classes.title }}>Schedule</Typography>
                <Typography classes={{ root: classes.caption }}>Config your event</Typography>

                <Paper elevation={0} className={classes.paperName}>
                    <div className={classes.nameWrapper}>
                        <div className={classes.nameContainer}>
                            <div className={classes.info}>
                                <Typography classes={{ root: classes.name }}>{event?.title}</Typography>

                                <div className={classes.timeWrapper}>
                                    <EventIcon className={classes.eventIcon} />
                                    <Typography variant="h5" className={classes.date} component="p">
                                        {parseDate(event?.time)}
                                    </Typography>
                                </div>
                                <Divider className={classes.divider} />
                                <div>
                                    <Typography className={classes.recurring} component="p">
                                        <span className={classes.recurringEvent}>Recurring event: </span>
                                        {parseDate(event?.time)}
                                    </Typography>
                                </div>
                            </div>
                            <Button variant="text" classes={{ root: classes.changeButton }}>
                                Change
                            </Button>
                        </div>
                    </div>
                </Paper>

                <Paper elevation={0} className={classes.paperPresenter}>
                    <Typography classes={{ root: classes.presenterTitle }}>Presenter</Typography>
                    <Typography classes={{ root: classes.presenterCaption }}>
                        You can invite upto 3 co-host and unlimited moderators to present this webinar
                    </Typography>
                    <div className={classes.infoPresenter}>
                        <div className={classes.presenterInfo}>
                            <Avatar src={event?.avatar} className={classes.avatar} />
                            <Typography
                                classes={{ root: classes.namePresenter }}
                            >{`${event?.firstName} ${event?.lastName}`}</Typography>
                            <Typography classes={{ root: classes.positionPresenter }}>{event?.role}</Typography>
                        </div>
                        <a className={classes.joinLink} href={event?.link}>
                            Join link
                        </a>
                    </div>
                </Paper>

                <Paper elevation={0} className={classes.paperDescription}>
                    <Typography classes={{ root: classes.descriptionTitle }}>Event description</Typography>
                    <Typography classes={{ root: classes.description }}>
                        This description appears on your registration page. Give as much detail as you need
                    </Typography>

                    <Paper className={classes.paperAddDescription}>
                        <Typography classes={{ root: classes.addDescription }}>No description added yet</Typography>
                        <Button variant="text">Add description</Button>
                    </Paper>
                </Paper>

                <Paper elevation={0} className={classes.paperSession}>
                    <Typography classes={{ root: classes.descriptionTitle }}>
                        Complete this before your session
                    </Typography>
                    <Typography classes={{ root: classes.sessionDescriptionCaption }}>
                        What are your key outcomes from our coaching together?
                    </Typography>
                    <TextField classes={{ root: classes.textField }} placeholder="Your session..." />
                </Paper>
            </div>
        </div>
    );
};

export default EventDetail;
