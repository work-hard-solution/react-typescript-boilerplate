import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    eventDetailWrapper: {},
    backToEvent: {
        display: 'flex',
        alignItems: 'center',
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '12px !important',
        color: '#000 !important',
        marginBottom: '24px !important',
    },
    arrowBackIcon: {
        marginRight: '8px !important',
        fontSize: '14px !important',
        color: '#000 !important',
    },
    paperName: {
        borderRadius: '8px !important',
        padding: '24px !important',
        marginBottom: '24px !important',
    },
    title: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '22px !important',
        color: '#000 !important',
        marginBottom: '4px !important',
    },
    caption: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
        marginBottom: '14px !important',
    },
    nameWrapper: {},
    nameContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    eventIcon: {
        marginRight: '8px',
    },

    info: {},
    timeWrapper: {
        display: 'flex',
    },
    date: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '18px !important',
        color: '#000 !important',
    },
    name: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '20px !important',
        color: '#000 !important',
        marginBottom: '16px !important',
    },
    recurring: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
        color: '#000 !important',
    },
    recurringEvent: {
        fontFamily: `${theme.typography.medium}`,
        fontSize: '14px',
        color: '#000 !important',
    },
    divider: {
        marginTop: '12px !important',
        marginBottom: '12px !important',
        width: '343px',
    },
    changeButton: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '16 !important',
    },
    paperPresenter: {
        borderRadius: '8px !important',
        padding: '24px !important',
        margin: 'auto 0 24px auto !important',
    },
    paperDescription: {
        borderRadius: '8px !important',
        padding: '24px !important',
        marginBottom: '24px !important',
    },
    presenterTitle: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '18 !important',
        color: '#000 !important',
        marginBottom: '8px !important',
    },
    presenterCaption: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
        color: '#000 !important',
        margin: 'auto 0 24px auto !important',
    },
    infoPresenter: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    presenterInfo: {
        display: 'flex',
    },
    avatar: {
        marginRight: '12px',
    },
    namePresenter: {
        fontFamily: `${theme.typography.medium} !important`,
        margin: 'auto 24px auto 0 !important',
        fontSize: '16px !important',
    },
    positionPresenter: {
        margin: 'auto 0 !important',
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '16px !important',
    },
    joinLink: {
        fontFamily: `${theme.typography.medium}`,
        textDecoration: 'none',
        fontSize: '14px',
        color: '#4393fc',
    },
    descriptionTitle: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '18 !important',
        color: '#000 !important',
        marginBottom: '8px !important',
    },
    description: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
        color: '#000 !important',
        margin: 'auto 0 24px auto !important',
    },
    addDescription: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
        color: '#000 !important',
        margin: 'auto 0 !important',
    },
    paperAddDescription: {
        display: 'flex',
        justifyContent: 'space-between',
        boxShadow: 'rgb(213 213 213 / 50%) 2px 2px 2px 2px !important',
        padding: '12px !important',
    },
    paperSession: {
        borderRadius: '8px !important',
        padding: '24px !important',
    },
    sessionDescriptionCaption: {
        fontFamily: `${theme.typography.regular} !important`,
        fontSize: '14px !important',
        color: '#000 !important',
        margin: 'auto 0 24px auto !important',
    },
    textField: {
        width: '568px',
        '& .MuiOutlinedInput-root': {
            borderRadius: '8px !important',
        },
    },
}));
