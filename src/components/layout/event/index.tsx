import { Box } from '@mui/material';
import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import AddEvent from './add';
import EventDetail from './detail';
import ListEvent from './list';

const Events: React.FC = () => {
    const match = useRouteMatch();

    return (
        <Box>
            <Switch>
                <Route path={match.path} exact>
                    <ListEvent />
                </Route>

                <Route path={`${match.path}/add`}>
                    <AddEvent />
                </Route>

                <Route path={`${match.path}/:eventId`}>
                    <EventDetail />
                </Route>
            </Switch>
        </Box>
    );
};

export default Events;
