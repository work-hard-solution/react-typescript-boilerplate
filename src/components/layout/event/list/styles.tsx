import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root1: {
        position: 'relative',
        borderRadius: '8px !important',
        marginBottom: '36px !important',
    },
    root2: {
        borderRadius: '8px !important',
    },

    // Version2
    searchWrapper: {
        padding: '24px',
    },
    toolBarTitle: {
        fontFamily: `${theme.typography.demi} !important`,
        fontSize: '16px !important',
        marginBottom: '12px !important',
    },
    buttonGroup: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    buttonAnalytics: {
        borderRadius: '8px !important',
        width: '100%',
        fontFamily: `${theme.typography.medium} !important`,
        color: `${theme.palette.common.white} !important`,
        backgroundColor: 'rgb(47, 129, 252) !important',
        fontSize: '14px !important',
    },
    listWrapper: {
        padding: '24px',
    },
    titleContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    titleList: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '16px !important',
    },
    totalEvent: {
        color: theme.palette.primary.main,
        fontFamily: `${theme.typography.heavy} !important`,
    },
    sortByWrapper: {},
    sortByText: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '16px !important',
    },
    listContainer: {},
}));
