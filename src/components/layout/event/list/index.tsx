import { Button, Paper, Typography } from '@mui/material';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { AnalyticsButton, EventCard, Filter } from 'components/partials';
import { ListParams } from 'models';
import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { eventActions, selectEventFilter, selectEventList } from 'redux/slices/event';

import { useStyles } from './styles';

const ListEvent: React.FC = () => {
    const match = useRouteMatch();
    const classes = useStyles();
    const dispatch = useAppDispatch();
    const filter = useAppSelector(selectEventFilter);
    const eventList = useAppSelector(selectEventList);

    React.useEffect(() => {
        dispatch(eventActions.fetchEventList(filter));
    }, [dispatch, filter]);

    const handleSearchChange = (newFilter: ListParams) => {
        dispatch(eventActions.setFilterWithDebounce(newFilter));
    };

    return (
        <div>
            <Paper elevation={0} classes={{ root: classes.root1 }}>
                <div className={classes.searchWrapper}>
                    <Typography classes={{ root: classes.toolBarTitle }}>Finding and booking your coaching</Typography>
                    <div className={classes.buttonGroup}>
                        <Filter filter={filter} onSearchChange={handleSearchChange} />
                        <AnalyticsButton />
                        <Link to={`${match.url}/add`} style={{ textDecoration: 'none', width: '20%' }}>
                            <Button variant="text" classes={{ root: classes.buttonAnalytics }} size="small">
                                Book new event
                            </Button>
                        </Link>
                    </div>
                </div>
            </Paper>
            <Paper elevation={0} classes={{ root: classes.root2 }}>
                <div className={classes.listWrapper}>
                    <div className={classes.titleContainer}>
                        <Typography classes={{ root: classes.titleList }}>
                            We have found <span className={classes.totalEvent}>40</span> events
                        </Typography>

                        <div className={classes.sortByWrapper}>
                            <Typography classes={{ root: classes.sortByText }}>Sort by</Typography>
                            <Button></Button>
                        </div>
                    </div>
                    <div className={classes.listContainer}>
                        {eventList.map((item) => (
                            <EventCard
                                key={item.id}
                                location={item?.link}
                                id={item.id}
                                eventName={item?.title}
                                unixTime={item?.time}
                                lastName={item?.lastName}
                                firstName={item?.firstName}
                                position={item?.role}
                                avatar={item?.avatar}
                            />
                        ))}
                    </div>
                </div>
            </Paper>
        </div>
    );
};

export default ListEvent;
