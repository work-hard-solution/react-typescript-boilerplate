import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: any) => ({
    wrapper: {
        width: '100%',
        '& .MuiGrid-container': {
            padding: 0,
        },
    },
    layout: {
        width: '100%',
        maxWidth: '380px !important',
        margin: '80px auto !important',
        textAlign: 'center',
    },
    marginLayout: {
        margin: 'auto',
    },
    images: {
        width: '33.33%',
        height: '100vh',
        objectFit: 'cover',
        position: 'fixed',
    },
    logoContainer: {
        width: '50%',
        margin: 'auto auto 30px auto',
    },
    logo: {
        width: '100%',
    },
    backgroundWrapper: {
        [theme.breakpoints.down(568)]: {
            display: 'none !important',
        },
    },
}));
