import { Grid } from '@mui/material';
import { images } from 'assets/images';
import React from 'react';

import { useStyles } from './styles';

const AuthLayout: React.FC = ({ children }) => {
    const customClasses = useStyles();

    return (
        <Grid classes={{ root: customClasses.wrapper }} spacing={0} container>
            <Grid xs={4} item classes={{ root: customClasses.backgroundWrapper }}>
                <img alt="background" className={customClasses.images} src={images.login_background.url} />
            </Grid>
            <Grid classes={{ root: customClasses.layout }} xs={8} item>
                <div className={customClasses.logoContainer}>
                    <img alt="logo" className={customClasses.logo} src={images.main_logo.src} />
                </div>
                {children}
            </Grid>
        </Grid>
    );
};
export default AuthLayout;
