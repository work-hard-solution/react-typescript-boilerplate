import { createTheme } from '@mui/material/styles';

import AvenirNextLTProDemiOTF from '../fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Demi.otf';
import AvenirNextLTProHeavyOTF from '../fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Heavy.otf';
import AvenirNextLTProMediumOTF from '../fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Medium.otf';
import AvenirNextLTProRegularOTF from '../fonts/Avenir_Next_Pro_Full/AvenirNextLTPro-Regular.otf';

const AvenirNextLTProHeavy = `@font-face {
    font-family: 'Avenir Next Heavy';
    src: url(${AvenirNextLTProHeavyOTF}) format("opentype");
}`;

const AvenirNextLTProDemi = `@font-face {
    font-family: 'Avenir Next Demi';
    src: url(${AvenirNextLTProDemiOTF}) format("opentype");
}`;

const AvenirNextLTProMedium = `@font-face {
    font-family: 'Avenir Next Medium';
    src: url(${AvenirNextLTProMediumOTF}) format("opentype");
}`;

const AvenirNextLTProRegular = `@font-face {
    font-family: 'Avenir Next Regular';
    src: url(${AvenirNextLTProRegularOTF}) format("opentype");
}`;

declare module '@mui/material/styles/createTypography' {
    interface TypographyOptions {
        heavy: string;
        demi: string;
        medium: string;
        regular: string;
    }
    interface Typography {
        heavy: string;
        demi: string;
        medium: string;
        regular: string;
    }
}

const theme = createTheme({
    palette: {
        primary: {
            main: '#4393fc',
            dark: '#3765d0',
            contrastText: 'rgb(67, 197, 252)',
        },
    },
    typography: {
        heavy: 'Avenir Next Heavy',
        demi: 'Avenir Next Demi',
        medium: 'Avenir Next Medium',
        regular: 'Avenir Next Regular',
        h3: {
            fontFamily: 'Avenir Next Demi',
            fontSize: 22,
            color: '#545458',
        },
        h4: {
            fontFamily: 'Avenir Next Demi',
            fontSize: 18,
            color: '#545458',
        },
        h5: {
            fontFamily: 'Avenir Next Demi',
            fontSize: 16,
            color: '#545458',
        },
        body2: {
            fontFamily: 'Avenir Next Demi',
            fontSize: 14,
            color: '#545458',
        },
        caption: {
            fontFamily: 'Avenir Next Medium',
            fontSize: 14,
            color: '#545458',
        },
        overline: {
            fontFamily: 'Avenir Next Regular',
            fontSize: 14,
            color: 'rgb(107, 114, 124)',
            lineHeight: '19px',
            textTransform: 'none',
            letterSpacing: 0,
        },
    },
    components: {
        MuiCssBaseline: {
            styleOverrides: `
            ${AvenirNextLTProHeavy}
            ${AvenirNextLTProDemi}
            ${AvenirNextLTProMedium}
            ${AvenirNextLTProRegular}
          `,
        },
        MuiButton: {
            variants: [
                {
                    props: { variant: 'text' },
                    style: {
                        textTransform: 'capitalize',
                        '&:hover': {
                            backgroundColor: 'transparent',
                        },
                    },
                },
            ],
        },
        MuiInput: {},
    },
});

export default theme;
