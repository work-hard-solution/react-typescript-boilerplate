/* eslint-disable global-require */

import loginBackground from './auth/auth-login-background.jpg';
import HomeIcon from './icons//home.svg';
// Icons
import githubIcon from './icons/github.svg';
import googleIcon from './icons/google.svg';
import linkedinIcon from './icons/linkedin.svg';
import MainLogin from './icons/logo.png';
import ProjectIcon from './icons/projects.svg';
import EventIcon from './icons/report.svg';
import logo from './icons/work-hard-logo.png';

export const images = {
    login_background: {
        url: loginBackground,
    },
    register_background: {
        url: require('./auth/auth.svg'),
    },
    forget_background: {
        url: require('./auth/forget.svg'),
    },
    domore_background: {
        url: require('./domore.png'),
    },
    // Icons
    main_logo: {
        src: MainLogin,
    },
    google_icon: {
        src: googleIcon,
    },
    github_icon: {
        src: githubIcon,
    },
    linkedin_icon: {
        src: linkedinIcon,
    },
    logo: {
        src: logo,
    },
    home_icon: {
        src: HomeIcon,
    },
    event_icon: {
        src: EventIcon,
    },
    project_icon: {
        src: ProjectIcon,
    },
};

export const imgSizes = {
    icon: '20px',
    avatar: '32px',
    avatarLarge: '48px',
    button: '48px',
};
