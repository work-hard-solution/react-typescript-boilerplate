import authSaga from 'redux/saga/auth';
import eventSaga from 'redux/saga/event';
import userSaga from 'redux/saga/user';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
    yield all([authSaga(), userSaga(), eventSaga()]);
}
