import { Action, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import rootSaga from 'app/rootSaga';
import { history } from 'configs/history';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { AuthReducer, EventReducer, UserReducer } from 'redux/slices';
import createSagaMiddleware from 'redux-saga';

const rootReducer = combineReducers({
    router: connectRouter(history),
    auth: AuthReducer,
    user: UserReducer,
    event: EventReducer,
});

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware, routerMiddleware(history)),
});

sagaMiddleware.run(rootSaga);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
