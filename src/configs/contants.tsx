export const APP_NAME = process.env.REACT_APP_NAME || 'React TypeScript Boilerplate';
// GOOGLE
export const GOOGLE_AUTHORIZATION_URL = 'https://accounts.google.com/o/oauth2/v2/auth';
export const GOOGLE_TOKEN_URL = 'https://oauth2.googleapis.com/token';

// LINKED IN
export const LINKED_IN_AUTHORIZATION_URL = 'https://www.linkedin.com/oauth/v2/authorization';
export const LINKED_IN_TOKEN_URL = 'https://www.linkedin.com/oauth/v2/accessToken';

// GITHUB
export const GITHUB_AUTHORIZATION_URL = 'https://github.com/login/oauth/authorize';
export const GITHUB_TOKEN_URL = 'https://github.com/login/oauth/access_token';
