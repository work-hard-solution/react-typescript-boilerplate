import { APP_NAME } from 'configs/contants';
import React, { Suspense } from 'react';
import { Helmet } from 'react-helmet';
import { Redirect, Route, Switch } from 'react-router-dom';
import { routes } from 'routes';
import PrivateRoute from 'routes/PrivateRoute';

function App() {
    return (
        <div className="App">
            <Helmet defaultTitle={APP_NAME} titleTemplate={`${APP_NAME} | %s`} />

            <Suspense fallback={<div>loading...</div>}>
                <Switch>
                    {routes.map((route) =>
                        route.auth ? (
                            <PrivateRoute
                                key={route.path}
                                component={route.component}
                                exact={route.exact}
                                path={route.path}
                            />
                        ) : (
                            <Route key={route.path} component={route.component} exact={route.exact} path={route.path} />
                        ),
                    )}
                    {/* <Redirect to="/not-found" /> */}
                </Switch>
            </Suspense>
        </div>
    );
}

export default App;
