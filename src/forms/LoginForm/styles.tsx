import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: any) => ({
    loginFormWrapper: {
        display: 'flex',
        margin: '20px 0 40px 0',
        justifyContent: 'center',
        fontSize: '16px',
    },
    welcome: {
        fontSize: '20px !important',
        fontFamily: `${theme.typography.medium} !important`,
    },
    linkExtend: {
        fontFamily: `${theme.typography.medium} !important`,
        fontSize: '15px !important',
        color: '#254D8E !important',
        lineHeight: '22px !important',
        marginLeft: '5px !important',
    },
    forgotPassword: {
        width: '100%',
        textAlign: 'right',
        fontFamily: `${theme.typography.regular} !important`,
        color: '#254D8E',
        fontSize: '16px !important',
        marginTop: '20px !important',
    },
    errorMessage: {
        marginBottom: '15px',
        textAlign: 'center',
    },
    containerEmail: {
        width: '100%',
        marginBottom: '30px',
        '& .MuiFormControl-root': {
            width: '100%',
        },
    },
    email: {
        borderRadius: '8px !important',
    },
    containerPassword: {
        width: '100%',
        '& .MuiFormControl-root': {
            width: '100%',
        },
    },
    signIn: {
        width: '100% !important',
        fontFamily: `${theme.typography.demi} !important`,
        marginTop: '40px !important',
        padding: '10px 27px !important',
    },
    signInButton: {
        width: '100% !important',
        color: `${theme.palette.common.white} !important`,
        fontFamily: `${theme.typography.demi} !important`,
        padding: '8px 12px !important',
        borderRadius: '8px !important',
        backgroundColor: `${theme.palette.primary.main} !important`,
        fontSize: '14px !important',
    },
    captionExtended: {
        textAlign: 'left',
        marginTop: '15px',
    },
    groupAuth: {
        marginTop: '11px',
        justifyContent: 'space-between',
        display: 'flex',
    },
    oauth2: {
        padding: '18px 37px !important',
        boxShadow: 'none !important',
        border: '1px solid #0C192D !important',
        backgroundColor: 'transparent !important',
        '&:hover': {
            backgroundColor: 'rgb(227, 230, 232) !important',
        },
    },
    term: {
        color: 'rgba(12,25,45,0.57) !important',
        marginTop: '40px !important',
        fontSize: '13px !important',
    },
    termAndPolicy: {
        marginLeft: '5px !important',
        color: '#254D8E !important',
        textDecoration: 'none !important',
    },
}));
