import { yupResolver } from '@hookform/resolvers/yup';
import { Grid, Typography } from '@mui/material';
import { images } from 'assets/images';
import { Button, Input, Link, Password } from 'components/base';
import React from 'react';
import { useForm } from 'react-hook-form';
import { LoginPayload } from 'redux/slices/auth';

// Styles
import { useStyles } from './styles';
// Validation
import { LoginSchema } from './validation';

interface ILoginForm {
    onSubmit: (values: LoginPayload) => void;
    initialValues: IForm;
}
export interface IForm {
    id: string;
    password: string;
}

const LoginForm: React.FC<ILoginForm> = ({ initialValues, onSubmit }) => {
    const classes = useStyles();

    const {
        control,
        handleSubmit,
        formState: { isSubmitting },
    } = useForm<IForm>({
        defaultValues: initialValues,
        resolver: yupResolver(LoginSchema),
    });

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Typography classes={{ root: classes.welcome }}>Welcome back</Typography>

            <Grid classes={{ root: classes.loginFormWrapper }} container>
                <Typography variant="caption">New to CoachingCloud? </Typography>
                <Link className={classes.linkExtend} to="/signup" blank={false}>
                    Sign up
                </Link>
            </Grid>

            <div className={classes.containerEmail}>
                <Input
                    name="id"
                    control={control}
                    type="text"
                    placeholder="Your email address"
                    className={classes.email}
                />
            </div>
            <div className={classes.containerPassword}>
                <Password name="password" control={control} />
            </div>
            <Button type="submit" loading={isSubmitting} disabled={isSubmitting} className={classes.signInButton}>
                Sign in
            </Button>
            <Grid container>
                <Link className={classes.forgotPassword} to="/forgot-password" blank={false}>
                    Forgot your password?
                </Link>
            </Grid>
            <div className={classes.captionExtended}>
                <Typography color="light" variant="caption" align="left">
                    Or login with
                </Typography>
            </div>

            {/* Login by oauth2 */}
            <div className={classes.groupAuth}>
                <Button className={classes.oauth2} src={images.linkedin_icon.src} alt="linkedin" />

                <Button className={classes.oauth2} src={images.google_icon.src} alt="google" />

                <Button className={classes.oauth2} src={images.github_icon.src} alt="github" />
            </div>
            <Typography classes={{ root: classes.term }} align="left">
                By continuing, you’re agreeing to our
                <Link className={classes.termAndPolicy} to="/term" blank={false}>
                    Terms of Use
                </Link>
                and
                <Link className={classes.termAndPolicy} to="/policy" blank={false}>
                    Privacy policy
                </Link>
            </Typography>
        </form>
    );
};

export default LoginForm;
