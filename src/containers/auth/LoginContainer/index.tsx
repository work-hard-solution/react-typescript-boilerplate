import { useAppDispatch } from 'app/hooks';
import { LoginForm } from 'forms';
import { IForm } from 'forms/LoginForm';
import React from 'react';
import { SubmitHandler } from 'react-hook-form';
import { authActions, LoginPayload } from 'redux/slices/auth';

const initialValues: IForm = {
    id: '',
    password: '',
};

const LoginContainer: React.FC = () => {
    const dispatch = useAppDispatch();

    const handleSubmit: SubmitHandler<LoginPayload> = async (values) => {
        dispatch(
            authActions.login({
                id: values.id,
                password: values.password,
            }),
        );
    };

    return <LoginForm initialValues={initialValues} onSubmit={handleSubmit} />;
};

export default LoginContainer;
