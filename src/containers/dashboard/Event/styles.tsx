import { Theme } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    wrapper: {
        width: '100%',
        padding: theme.spacing(2),
    },
    titlePaper: {
        padding: theme.spacing(2),
    },
    title: {
        fontSize: '14px !important',
        fontFamily: `${theme.typography.demi} !important`,
        lineHeight: '19px !important',
        color: 'rgb(12, 25, 45) !important',
        marginBottom: '24px !important',
    },
    caption: {
        fontFamily: `${theme.typography.medium} !important`,
        color: 'rgb(107, 114, 124) !important',
        fontSize: '14px !important',
        lineHeight: '19px !important',
    },
    datePickerWrapper: {
        marginRight: '48px !important',
    },
}));
