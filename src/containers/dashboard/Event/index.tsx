import { Grid, Paper, Typography } from '@mui/material';
import { DatePicker } from 'components/base';
import { EventCard } from 'components/partials';
import React from 'react';

import { useStyles } from './styles';

const Events: React.FC = () => {
    const customClasses = useStyles();

    return (
        <Paper elevation={0}>
            <div className={customClasses.titlePaper}>
                <Typography className={customClasses.title}>GENERAL</Typography>
                <Typography className={customClasses.caption}>Finding and booking your coaching</Typography>
            </div>
            <Grid classes={{ root: customClasses.wrapper }} container>
                <Grid classes={{ item: customClasses.datePickerWrapper }} item>
                    <DatePicker />
                </Grid>
                <Grid item>{/* <EventCard /> */}</Grid>
            </Grid>
        </Paper>
    );
};

export default Events;
