/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import request from 'utils/request';

export const getUserInfoApi = () => {
    return request.get('/users');
};
