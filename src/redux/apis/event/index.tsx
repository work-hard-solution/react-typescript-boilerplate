import { IEvent, ListParams, ListResponse } from 'models';
import requestV2 from 'utils/requestV2';

const eventApi = {
    getAll(params: ListParams): Promise<ListResponse<IEvent>> {
        const url = '/event';
        return requestV2.get(url, { params });
    },
    getById(id: string): Promise<IEvent> {
        const url = `/event/${id}`;
        return requestV2.get(url);
    },
    add(data: IEvent): Promise<IEvent> {
        const url = '/event';
        return requestV2.post(url, data);
    },
    update(data: IEvent): Promise<IEvent> {
        const url = '/event';
        return requestV2.patch(url, data);
    },
    remove(id: string): Promise<any> {
        const url = `/event/${id}`;
        return requestV2.delete(url);
    },
};

export default eventApi;
