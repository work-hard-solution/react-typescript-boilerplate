import React from 'react';
import { LoginPayload, RegisterPayload } from 'redux/slices/auth';
import request from 'utils/request';

export const loginApi: React.FC<LoginPayload> = (payload) => {
    return request.post('/auth/login', payload);
};

export const registerApi: React.FC<RegisterPayload> = (payload) => {
    return request.post('/auth/register', payload);
};
