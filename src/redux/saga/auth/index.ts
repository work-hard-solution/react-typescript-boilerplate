import { PayloadAction } from '@reduxjs/toolkit';
import { push } from 'connected-react-router';
import { IUser } from 'models';
import { loginApi } from 'redux/apis/auth';
import { authActions, LoginPayload } from 'redux/slices/auth';
import { call, fork, put, take } from 'redux-saga/effects';

function* handleLogin(payload: LoginPayload) {
    try {
        const response: IUser = yield call(loginApi, payload);
        if (response.tokens.access_token) localStorage.setItem('access_token', response?.tokens?.access_token);

        yield put(authActions.loginSuccess(response));
        yield put(push('/admin'));

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (error: any) {
        console.log('error', error);
        yield put(authActions.loginFailed(error));
    }
}

function* handleLogout() {
    localStorage.removeItem('access_token');
    yield put(push('/login'));
}

function* watchLoginFlow() {
    while (true) {
        const isLoggedIn = Boolean(localStorage.getItem('access_token'));

        if (!isLoggedIn) {
            const action: PayloadAction<LoginPayload> = yield take(authActions.login.type);
            yield fork(handleLogin, action.payload);
        }

        yield take(authActions.logout.type);
        yield call(handleLogout);
    }
}

export default function* authSaga() {
    yield fork(watchLoginFlow);
}
