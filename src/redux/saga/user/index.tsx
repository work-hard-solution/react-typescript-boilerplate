import { PayloadAction } from '@reduxjs/toolkit';
import { isEmpty } from 'lodash';
import { IUserInfo } from 'models';
import { getUserInfoApi } from 'redux/apis/user';
import { GetInfoPayload, userActions } from 'redux/slices/user';
import { call, fork, put, take } from 'redux-saga/effects';

function* handleGetUserInfo(payload: GetInfoPayload) {
    try {
        const user: IUserInfo = yield call(getUserInfoApi);
        if (!isEmpty(user)) yield put(userActions.getUserInfoSuccess(user));
    } catch (error: any) {
        yield put(userActions.getUserFailed(error));
    }
}

export default function* userSaga() {
    const action: PayloadAction<GetInfoPayload> = yield take(userActions.getUserInfo.type);
    yield fork(handleGetUserInfo, action.payload);
}
