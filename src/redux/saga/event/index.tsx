import { PayloadAction } from '@reduxjs/toolkit';
import { IEvent, ListParams, ListResponse } from 'models';
import eventApi from 'redux/apis/event';
import { eventActions } from 'redux/slices/event';
import { call, debounce, put, takeLatest } from 'redux-saga/effects';

function* fetchEventList(action: PayloadAction<ListParams>) {
    try {
        const response: ListResponse<IEvent> = yield call(eventApi.getAll, action.payload);
        yield put(eventActions.fetchEventListSuccess(response));
    } catch (error: any) {
        console.log('Failed to fetch event list', error);
        yield put(eventActions.fetchEventListFailed(error));
    }
}

function* handleSearchDebounce(action: PayloadAction<ListParams>) {
    yield put(eventActions.setFilter(action.payload));
}

export default function* eventSaga() {
    yield takeLatest(eventActions.fetchEventList, fetchEventList);

    yield debounce(500, eventActions.setFilterWithDebounce.type, handleSearchDebounce);
}
