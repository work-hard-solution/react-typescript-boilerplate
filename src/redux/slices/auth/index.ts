/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IUser } from 'models';

import { NAME } from '../constants';

export interface LoginPayload {
    id: string;
    password: string;
}

export interface RegisterPayload {
    first_name: string;
    last_name: string;
    email: string;
    password: string;
}
export interface AuthState {
    isLoggedIn: boolean;
    logging?: boolean;
    currentUser?: IUser;
}

const initialState: AuthState = {
    isLoggedIn: false,
    logging: false,
    currentUser: undefined,
};

const authSlice = createSlice({
    name: NAME.AUTH,
    initialState,
    reducers: {
        // Login
        login(state, action: PayloadAction<LoginPayload>) {
            state.logging = true;
        },
        loginSuccess(state, action: PayloadAction<IUser>) {
            console.log('action___', action.payload);
            state.isLoggedIn = true;
            state.logging = false;
            state.currentUser = action.payload;
        },
        loginFailed(state, action: PayloadAction<string>) {
            state.logging = false;
        },
        logout(state) {
            state.isLoggedIn = false;
            state.currentUser = undefined;
        },

        // Register
        register(state, action: PayloadAction<RegisterPayload>) {
            state.logging = true;
        },
        registerSuccess(state, action: PayloadAction<IUser>) {
            state.currentUser = action.payload;
        },
        registerFailed(state, action: PayloadAction<string>) {
            state.currentUser = undefined;
        },
    },
});

// Actions
export const authActions = authSlice.actions;

export const selectIsLoggedIn = (state: any) => state.auth.isLoggedIn;
export const selectIsLogging = (state: any) => state.auth.logging;
export const selectCurrentUser = (state: any) => state.auth.currentUser;

const authReducer = authSlice.reducer;

export default authReducer;
