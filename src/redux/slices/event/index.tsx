import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/store';
import { IEvent, ListParams, ListResponse, PaginationParams } from 'models';

import { NAME } from '../constants';

export interface EventState {
    loading: boolean;
    list: IEvent[];
    filter: ListParams;
    pagination?: PaginationParams;
}

const initialState: EventState = {
    loading: false,
    list: [],
    filter: {
        page: 1,
        limit: 15,
    },
    pagination: {
        page: 1,
        limit: 15,
        totalRows: 15,
    },
};

const eventSlice = createSlice({
    name: NAME.EVENT,
    initialState,
    reducers: {
        fetchEventList(state, action: PayloadAction<ListParams>) {
            state.loading = true;
        },

        fetchEventListSuccess(state, action: PayloadAction<ListResponse<IEvent>>) {
            state.list = action.payload.data;
            state.pagination = action.payload.pagination;
            state.loading = false;
        },
        fetchEventListFailed(state, action: PayloadAction<ListParams>) {
            state.loading = false;
        },

        setFilter(state, action: PayloadAction<ListParams>) {
            state.filter = action.payload;
        },

        // eslint-disable-next-line @typescript-eslint/no-empty-function
        setFilterWithDebounce(state, action: PayloadAction<ListParams>) {},
    },
});

export const eventActions = eventSlice.actions;

export const selectEventList = (state: RootState) => state.event.list;
export const selectEventLoading = (state: RootState) => state.event.loading;
export const selectEventFilter = (state: RootState) => state.event.filter;
export const selectEventPagination = (state: RootState) => state.event.pagination;

const eventReducer = eventSlice.reducer;

export default eventReducer;
