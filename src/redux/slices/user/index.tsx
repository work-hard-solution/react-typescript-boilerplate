import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IUserInfo } from 'models';

import { NAME } from '../constants';

export interface GetInfoPayload {
    access_token: string;
}

export interface UserState {
    isLoggedIn: boolean;
    logging?: boolean;
    currentUser?: IUserInfo;
}

const initialState: UserState = {
    isLoggedIn: false,
    logging: false,
    currentUser: undefined,
};

const userSlice = createSlice({
    name: NAME.USER,
    initialState,
    reducers: {
        getUserInfo(state) {
            state.logging = true;
        },
        getUserInfoSuccess(state, action: PayloadAction<IUserInfo>) {
            state.isLoggedIn = true;
            state.logging = false;
            state.currentUser = action.payload;
        },
        getUserFailed(state, action: PayloadAction<string>) {
            state.logging = false;
        },
    },
});

// Actions
export const userActions = userSlice.actions;

export const selectCurrentUser = (state: any) => state.auth.currentUser;

const userReducer = userSlice.reducer;

export default userReducer;
