export interface IUser {
    user: {
        id: string;
        name: string;
        email: string;
        email_verified: boolean;
        type: string;
        status: string;
        created_at: string;
    };
    tokens: {
        token_type: string;
        access_token: string;
        expires_in: number;
        refresh_token: string;
    };
}

export interface IUserInfo {
    id: string;
    name: string;
    email: string;
    email_verified: boolean;
    type: string;
}
